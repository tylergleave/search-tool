<x-guest-layout>
    @section('title', 'Upload Your Tracks')

    <x-slot name="header">
            Upload Your Tracks
    </x-slot>

    <div class="max-w-7xl mx-auto px-6 lg:px-8 py-5 items-center">
        <a class="underline text-blue-500" href="/">Back to Map</a>
    </div>
    <div class="max-w-7xl mx-auto px-6 lg:px-8">
        <p>
            You can upload your .gpx, .xml, or .kml tracks here and they will be overlaid on the map.
        </p>
        <p>
            Please upload ONLY .gpx, .xml, or .kml format files from Gaia, Strava, All Trails or other GPS apps. <span class="text-red-500 font-bold underline">Screenshots from your phone will not work.</span>
        </p>
        <p class="mt-5">
            For help exporting files from Strava, <a class="text-blue-500 underline" href="https://support.strava.com/hc/en-us/articles/216918437-Exporting-your-Data-and-Bulk-Export#GPX" target="_BLANK">click here</a>.
        </p>
        <p>
            For help exporting files from GaiaGPS, <a class="text-blue-500 underline" href="https://help.gaiagps.com/hc/en-us/articles/115003524687-Export-Data-as-GPX-KML-or-GeoJSON-from-gaiagps-com" target="_BLANK">click here</a>.
        </p>
        <p>
            For help exporting files from All Trails, <a class="text-blue-500 underline" href="https://support.alltrails.com/hc/en-us/articles/360018930772-How-do-I-export-a-file-from-AllTrails-" target="_BLANK">click here</a>.
        </p>
    </div>
    <div class="py-12">
        <div class="max-w-7xl mx-auto px-6 lg:px-8">
            <div class="bg-white overflow-hidden">
                <form method="POST" action="/upload" enctype="multipart/form-data">
                    @csrf
                    <div class="mb-5">
                        <input type="file" name="gpx" accept=".gpx,.kml,.xml" required>
                    </div>
                    <div class="mb-3">
                        @error('gpx') <span class="text-red-500 font-bold">{{ $message }}</span> @enderror
                    </div>
                    <button type="submit" class="mt-5 px-5 py-2 rounded bg-gray-200">Upload GPX File</button>
                </form>
            </div>
        </div>
    </div>


    @section('scripts')

    @endsection
</x-guest-layout>
