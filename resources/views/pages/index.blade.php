<x-guest-layout>
    @section('title', 'Help Find Shelby Campbell')
    <x-slot name="header">
            Help Find Shelby Campbell
    </x-slot>
    <div>
        <p class="text-center"><span class="underline font-bold">{{ count($files) }}</span> search tracks uploaded so far</p>
    </div>
    <div class="pt-4 flex content-center">
        <a class="bg-maroon-800 hover:bg-maroon-900 text-white font-bold py-2 px-4 rounded mx-auto" href="/upload">Click Here to Add Your Tracks</a>
    </div>

    <div class="max-w-7xl mx-auto px-6 lg:px-8 pt-4">
        <label for="trackingButton">
            <input id="trackingButton" type="checkbox" onclick="handleChange(this);"/>
            Show My Location on the Map
        </label>
        <strong><span id="locationAccuracy" class="ml-2"></span></strong>

        <!--<button onclick="StartTracking();">Test</button> -->
    </div>

    <div class="py-4">
        <div class="max-w-7xl mx-auto px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <div id="map" class="w-full h-screen"></div>
            </div>
        </div>
    </div>

    @section('scripts')
    <script type='text/javascript'>
    var map, watchId, userPin;

    function loadMap(){
        map = new Microsoft.Maps.Map(document.getElementById('map'), {
            mapTypeId: Microsoft.Maps.MapTypeId.aerial,
            center: new Microsoft.Maps.Location(41.312831, -111.895203),
            zoom: 15
        });

        Microsoft.Maps.loadModule('Microsoft.Maps.GeoXml', function () {
            @foreach($files as $mapfile)
            var layer = new Microsoft.Maps.GeoXmlLayer('{{ url(Storage::url($mapfile)) }}');
            layer.setOptions({
                autoUpdateMapView:false,
                defaultStyles: {
                    polylineOptions: {
                        strokeColor: 'red',
                        strokeThickness: 3
                    }
                },
            });
            map.layers.insert(layer);
            @endforeach

            // Clear all pushpins created by users' KML
            map.entities.clear();

            var phoneLocation = new Microsoft.Maps.Location(41.312831, -111.895203);
            //Mark last cell phone contact
            var pin = new Microsoft.Maps.Pushpin(phoneLocation, {
                color: 'yellow',
                title: 'Last Known Cell Phone Location',
                subtitle: '41.312831, -111.895203',
                text: '1'
            });
            //Add the pushpin to the map
            map.entities.push(pin);
            });
    }

    function handleChange(checkbox){
        if(checkbox.checked == true){
            StartTracking();
        }else{
            StopTracking();
        }
    }

    function StartTracking() {
        //Add a pushpin to show the user's location.
        userPin = new Microsoft.Maps.Pushpin(map.getCenter(), { visible: false });
            map.entities.push(userPin);

        //Watch the users location.
        watchId = navigator.geolocation.watchPosition(UsersLocationUpdated, console.log('geolocation error'), {'enableHighAccuracy':true});
    }

    function UsersLocationUpdated(position) {
        var loc = new Microsoft.Maps.Location(
                    position.coords.latitude,
                    position.coords.longitude);

        //Update the user pushpin.
        userPin.setLocation(loc);
        userPin.setOptions({ visible: true });

        //Update the accuracy
        document.getElementById("locationAccuracy").textContent="(Accuracy: " + Math.round(position.coords.accuracy) + "m)";

        //Center the map on the user's location.
        map.setView({ center: loc });
    }

    function StopTracking() {
        var phoneLocation = new Microsoft.Maps.Location(41.312831, -111.895203);
        // Cancel the geolocation updates.
        navigator.geolocation.clearWatch(watchId);
        //Remove the user pushpin.
        map.entities.clear();
        //Mark last cell phone contact
        var pin = new Microsoft.Maps.Pushpin(phoneLocation, {
            color: 'yellow',
            title: 'Last Known Cell Phone Location',
            subtitle: '41.312831, -111.895203',
            text: '1'
        });
        //Add the pushpin to the map
        map.entities.push(pin);
        // Center map back on cell phone location
        map.setView({ center: phoneLocation });
        //Remove accuracy reading
        document.getElementById("locationAccuracy").textContent="";

    }
    </script>
            <script type='text/javascript' src='https://www.bing.com/api/maps/mapcontrol?key=Am7_34JSxppPY1gEb1KDe3yeyM851SaPxcIM7YE4_KJ5ei6iX70ky-jIV3jjIuoT&callback=loadMap' async defer></script>
    @endsection
</x-guest-layout>
