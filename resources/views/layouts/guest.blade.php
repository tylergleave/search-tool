<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
        
        @production
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-44597264-10"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-44597264-10');
        </script>
        @endproduction

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }} | @yield('title', 'Something about finding somebody')</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}?v=20200916">

        <!-- Scripts -->
        <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.2.1/dist/alpine.js" defer></script>
        
    </head>
    <body>
         <!-- Nav -->
         <nav class="bg-green shadow">
            <div class="py-6 w-full flex justify-center">
                <a href="/">
                    <img src="/img/search-for-logo.png" alt="Search For Us logo" style="height:40px;">
                </a>
            </div>
        </nav>

        <!-- Heading -->
        <header>
            <h2 class="font-semibold text-3xl text-center py-5">
                {{ $header }}
            </h2>
        </header>

        @if(session('success'))
        <!-- Flash Messaging -->
            <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative" role="alert">
            <span class="block sm:inline">{{ session('success') }}</span>
            </div>
        @endif

        <!-- Body Content -->
        <div class="font-sans text-gray-900 antialiased">
            {{ $slot }}
        </div>

        <!-- Footer-->
        <footer></footer>
        @yield('scripts')
    </body>
</html>
