<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FileController extends Controller
{
    public function add(){
        return view('pages.upload');
    }

    public function upload(Request $request){
        $request->validate([
            'gpx' => 'required|max:7168|mimes:gpx,xml,gpx+xml,kml',
        ]);

        $path = $request->file('gpx')->store('public/tracks');

        return redirect('/')->with('success', 'Your GPX file has been uploaded.');
    }
}
