<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PageController extends Controller
{
    public function index(){
        $files = Storage::files('/public/tracks');
        return view('pages.index', ['files' => $files]);
    }
}
